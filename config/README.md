# Configuration

## config.php
Main configuration file where you can configre which Ex Libris service environments you wich to use.

## keys.ini
Not provided in the repository, but it is necessary when using the Ex Libris API's.  The file should be created in this directory with the following entry for the Ex Libris API bibliographic read/write key.

``bib-key = "{key value}"``