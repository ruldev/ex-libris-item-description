<?php
$aConfig = array();
//set Ex Libris API configuration
$aConfig['ex-libris-api-host'] = "https://api-na.hosted.exlibrisgroup.com";
$aConfig['ex-libris-api-get-by-barcode'] = "/almaws/v1/items?item_barcode={item_barcode}";
$aConfig['ex-libris-api-get-by-item-id'] = "/almaws/v1/bibs/{mms_id}/holdings/{holding_id}/items/{item_pid}";
$aConfig['ex-libris-api-update-by-item-id'] = "/almaws/v1/bibs/{mms_id}/holdings/{holding_id}/items/{item_pid}";
$aConfig['ex-libris-api-key-var-name'] = "apikey";
$aConfig['keys'] = parse_ini_file(__DIR__ . '/../config/keys.ini');
$aConfig['ex-libris-api-key'] = $aConfig['keys']['bib-key'];
//if any fewer than this number of requests are left for the day the script will stop
$aConfig['ex-libris-api-threshold-header-variable'] = "X-Exl-Api-Remaining";
$aConfig['ex-libris-api-request-threshold'] = 50000;
