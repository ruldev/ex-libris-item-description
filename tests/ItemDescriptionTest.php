<?php
use PHPUnit\Framework\TestCase;
require __DIR__ . '/../src/ItemDescription.php';

class ItemDescriptionTest extends TestCase{
    //class
    public $itemDescriptionInstance;

    //empty data for testing
    public $generateData = array('a'=>'', 'b'=>'', 'c'=>'', 'd'=>'', 'e'=>'', 'f'=>'', 'g'=>'', 'h'=>'', 'i'=>'', 'j'=>'', 'k'=>'', 'l'=>'', 'm'=>'');
    public $parseData = array('pattern' => '', 'a'=>'', 'b'=>'', 'c'=>'', 'd'=>'', 'e'=>'', 'f'=>'', 'g'=>'', 'h'=>'', 'i'=>'', 'j'=>'', 'k'=>'', 'l'=>'', 'm'=>'');

    //start
    protected function setUp(): void {
        $this->itemDescriptionInstance = new ItemDescription();
    }

    /**
     * Generate tests
     **/

    //no data provided
    public function testGenerateIfEmpty(){
        $this->assertEquals(false, $this->itemDescriptionInstance->generateDescription(array()));
    }

    //nothing provided
    public function testGenerateIfNoValues(){
        $thisData = $this->generateData;
        $this->assertEquals(false, $this->itemDescriptionInstance->generateDescription($thisData));
    }

    //Year
    public function testGenerateYear(){
        $thisData = $this->generateData;
        //single year only
        $thisData['i'] = "2021";
        $this->assertEquals("2021", $this->itemDescriptionInstance->generateDescription($thisData));
        //multiple years
        $thisData['i'] = "2020/2021";
        $this->assertEquals("2020/2021", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "2020/";
        $this->assertEquals("2020", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "1999/2020/2021";
        $this->assertFalse($this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "2020\\2021";
        $this->assertFalse($this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "2020/202a";
        $this->assertFalse($this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "202/2021";
        $this->assertFalse($this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "2021/2020";
        $this->assertFalse($this->itemDescriptionInstance->generateDescription($thisData));
    }//END testYear

    //Month
    public function testGenerateMonth(){
        $thisData = $this->generateData;
        $thisData['i'] = "2021";//year
        $thisData['j'] = "01";//January
        $this->assertEquals("2021:Jan.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "02";//February
        $this->assertEquals("2021:Feb.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "03";//March
        $this->assertEquals("2021:Mar.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "04";//April
        $this->assertEquals("2021:Apr.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "05";//May
        $this->assertEquals("2021:May", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "06";//June
        $this->assertEquals("2021:June", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "07";//July
        $this->assertEquals("2021:July", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "08";//August
        $this->assertEquals("2021:Aug.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "09";//September
        $this->assertEquals("2021:Sept.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "10";//October
        $this->assertEquals("2021:Oct.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "11";//November
        $this->assertEquals("2021:Nov.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "12";//December
        $this->assertEquals("2021:Dec.", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "21";//spring
        $this->assertEquals("2021:Spring", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "22";//summer
        $this->assertEquals("2021:Summer", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "23";//autumn
        $this->assertEquals("2021:Fall", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "24";//winter
        $this->assertEquals("2021:Winter", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "1";//not a valid month
        $this->assertFalse($this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "25";//not a valid season
        $this->assertFalse($this->itemDescriptionInstance->generateDescription($thisData));
        //no year, but month
        $thisData = $this->generateData;
        $thisData['j'] = "01";
        $this->assertFalse($this->itemDescriptionInstance->generateDescription($thisData));
    }//END textYear

    //Volume
    public function testGenerateVolume(){
        //volume, number, part, year, month
        $thisData = $this->generateData;
        $thisData['a'] = "1";//volume
        $this->assertEquals("v.1", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['b'] = "2";//number
        $this->assertEquals("v.1:no.2", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['c'] = "3";//part
        $this->assertEquals("v.1:no.2:pt.3", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "2021";//year
        $this->assertEquals("v.1:no.2:pt.3(2021)", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "01";//month
        $this->assertEquals("v.1:no.2:pt.3(2021:Jan.)", $this->itemDescriptionInstance->generateDescription($thisData));
        //volume, part, year, month
        $thisData = $this->generateData;
        $thisData['a'] = "1";//volume
        $this->assertEquals("v.1", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['c'] = "3";//part
        $this->assertEquals("v.1:pt.3", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "2021";//year
        $this->assertEquals("v.1:pt.3(2021)", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "01";//month
        $this->assertEquals("v.1:pt.3(2021:Jan.)", $this->itemDescriptionInstance->generateDescription($thisData));
        //volume, year, month
        $thisData = $this->generateData;
        $thisData['a'] = "1";//volume
        $thisData['i'] = "2021";//year
        $this->assertEquals("v.1(2021)", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "01";//month
        $this->assertEquals("v.1(2021:Jan.)", $this->itemDescriptionInstance->generateDescription($thisData));
    }//END testVolume

    //Number
    public function testGenerateNumber(){
        //number, part, year, month
        $thisData = $this->generateData;
        $thisData['b'] = "2";//number
        $this->assertEquals("no.2", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['c'] = "3";//part
        $this->assertEquals("no.2:pt.3", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "2021";//year
        $this->assertEquals("no.2:pt.3(2021)", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "01";//month
        $this->assertEquals("no.2:pt.3(2021:Jan.)", $this->itemDescriptionInstance->generateDescription($thisData));
        //number, year, month
        $thisData = $this->generateData;
        $thisData['b'] = "2";//number
        $thisData['i'] = "2021";//year
        $this->assertEquals("no.2(2021)", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "01";//month
        $this->assertEquals("no.2(2021:Jan.)", $this->itemDescriptionInstance->generateDescription($thisData));
    }//END testNumber

    //Part
    public function testGeneratePart(){
        //part, year, month
        $thisData = $this->generateData;
        $thisData['c'] = "3";//part
        $this->assertEquals("pt.3", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['i'] = "2021";//year
        $this->assertEquals("pt.3(2021)", $this->itemDescriptionInstance->generateDescription($thisData));
        $thisData['j'] = "01";//month
        $this->assertEquals("pt.3(2021:Jan.)", $this->itemDescriptionInstance->generateDescription($thisData));
    }//END testPart

    //Series
    public function testGenerateSeries(){
        //series
        $thisData = $this->generateData;
        $thisData['d'] = "4";//part
        $this->assertNull($this->itemDescriptionInstance->generateDescription($thisData));
    }//END testSeries

    /**
     * range tests
     **/
    public function testRangeValues(){
        //test false
        $this->assertFalse($this->itemDescriptionInstance->parseRange(''));
        $this->assertFalse($this->itemDescriptionInstance->parseRange('1996/1995'));
        $this->assertFalse($this->itemDescriptionInstance->parseRange('1996/1996'));
        $this->assertFalse($this->itemDescriptionInstance->parseRange('1996-1995'));
        $this->assertFalse($this->itemDescriptionInstance->parseRange('1996-1996'));
        $this->assertFalse($this->itemDescriptionInstance->parseRange('1996-95'));
        $this->assertFalse($this->itemDescriptionInstance->parseRange('1995-01'));
        //test okay
        $this->assertEquals(array('value' => '1995/1996', 'pattern' =>'year-range'), $this->itemDescriptionInstance->normalizeYear('1995/1996'));
        $this->assertEquals(array('value' => '1995-1996', 'pattern' =>'year-range'), $this->itemDescriptionInstance->normalizeYear('1995-1996'));
        $this->assertEquals(array('value' => '1995-1997', 'pattern' =>'year-range'), $this->itemDescriptionInstance->normalizeYear('1995-97'));
        $this->assertEquals(array('value' => '1995-2000', 'pattern' =>'year-range'), $this->itemDescriptionInstance->normalizeYear('1995-00'));
        $this->assertEquals(array('value' => '1995-1996', 'pattern' =>'year-range'), $this->itemDescriptionInstance->normalizeYear('1995-96'));
    }//END testRangeValues

    /**
     * Parse tests
     **/
    //no data provided
    public function testParseIfEmpty(){
        $this->assertFalse($this->itemDescriptionInstance->parseDescription(''));
    }

    public function testParseVolumeOnly(){
        $thisData = $this->parseData;
        $thisData['a'] = '7';
        $thisData['pattern'] = '_volume-only';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.7'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V.7'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v. 7'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V. 7'));
        $thisData['a'] = '10/11';
        $thisData['pattern'] = '_volume-range';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.10/11'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V.10/11'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v. 10/11'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V. 10/11'));
        $thisData['a'] = '10-11';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.10-11'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V.10-11'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v. 10-11'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V. 10-11'));
        //failures
        $thisData = $this->parseData;
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.11-10'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V.11-10'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v. 11-10'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V. 11-10'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.11/10'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V.11/10'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v. 11/10'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V. 11/10'));
    }//END testParseVolumeOnly

    public function testParseVolumeYear(){
        $thisData = $this->parseData;
        $thisData['a'] = '1';
        $thisData['i'] = '1960';
        $thisData['pattern'] = '_volume-and-year-only';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v. 1 1960'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V. 1 1960'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 1960'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V.1 1960'));
        $thisData['a'] = '8';
        $thisData['i'] = '1994/1995';
        $thisData['pattern'] = '_volume-and-year-range';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v. 8 1994/1995'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V. 8 1994/1995'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.8 1994/1995'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V.8 1994/1995'));
        $thisData['i'] = '1994-1995';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.8 1994-1995'));
//        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.8 1994-95'));
        //failures
        $thisData = $this->parseData;
        $thisData['pattern'] = 'volume-and-year-invalid';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('V.8 1995/1994'));
    }//END testParseVolumeYear

    public function testParseVolumeNumber(){
        $thisData = $this->parseData;
        $thisData['a'] = '1';
        $thisData['b'] = '2';
        $thisData['pattern'] = '_volume-no';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 NO.2'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 No.2'));
        $thisData['b'] = '2-3';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2-3'));
        //failure
        $thisData = $this->parseData;
        $thisData['pattern'] = 'volume-no-invalid-volume';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1no.2'));
        $thisData['pattern'] = 'volume-unknown';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2-3 no.4-5'));
    }//END testParseVolumeNumber

    public function testParseVolumeNumberYear(){
        $thisData = $this->parseData;
        $thisData['a'] = '1';
        $thisData['b'] = '2';
        $thisData['i'] = '2001';
        $thisData['pattern'] = '_volume-no-year-only';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2 2001'));
        $thisData['b'] = '2-3';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2-3 2001'));
        $thisData['i'] = '2001/2002';
        $thisData['pattern'] = '_volume-no-year-range';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2-3 2001/2002'));
    }//END testParseVolumeNumberYear

    public function testParseVolumeNumberPart(){
        $thisData = $this->parseData;
        $thisData['a'] = '1';
        $thisData['b'] = '2';
        $thisData['c'] = '3';
        $thisData['pattern'] = '_volume-no-pt';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2 pt.3'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2 Pt.3'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2 PT.3'));
        $thisData['c'] = '3-4';
        $thisData['pattern'] = '_volume-no-pt';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2 pt.3-4'));
    }//END testParseVolumeNumberPart

    public function testParseVolumeNumberPartYear(){
        $thisData = $this->parseData;
        $thisData['a'] = '1';
        $thisData['b'] = '2';
        $thisData['c'] = '3';
        $thisData['i'] = '2001';
        $thisData['pattern'] = '_volume-no-pt-year-only';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 no.2 pt.3 2001'));
    }//END testParseVolumeNumberPartYear

    public function testParseVolumePart(){
        $thisData = $this->parseData;
        $thisData['a'] = '1';
        $thisData['c'] = '3';
        $thisData['pattern'] = '_volume-pt';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 pt.3'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 Pt.3'));
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 PT.3'));
        //failure
        $thisData = $this->parseData;
        $thisData['pattern'] = 'volume-pt-invalid-volume';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1pt.3'));
    }//END testParseVolumeNumber

    public function testParseVolumePartYear(){
        $thisData = $this->parseData;
        $thisData['a'] = '1';
        $thisData['c'] = '3';
        $thisData['i'] = '2001';
        $thisData['pattern'] = '_volume-pt-year-only';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 pt.3 2001'));
        //failure
        $thisData = $this->parseData;
        $thisData['pattern'] = 'volume-unknown';
        $this->assertEquals($thisData, $this->itemDescriptionInstance->parseDescription('v.1 pt.1-4 pt.5 2001'));
    }//END testParseVolumeNumber

    public function testUpdateInputFile(){
        //empty update-item file
        $this->assertFalse($this->itemDescriptionInstance->updateItems(__DIR__ . '/resources/update-file-empty.txt', false));
        //update-item file without a header
        $this->assertFalse($this->itemDescriptionInstance->updateItems(__DIR__ . '/resources/update-file-no-header.txt', false));
        //update-item file with invalid header
        $this->assertFalse($this->itemDescriptionInstance->updateItems(__DIR__ . '/resources/update-file-with-invalid-header.txt', false));
        //update-item file with valid header
        $this->assertIsArray($this->itemDescriptionInstance->updateItems(__DIR__ . '/resources/update-file-with-valid-header.txt', false));
    }//END testParseVolumeNumber

}//END class ItemDescriptionGenerationTest

