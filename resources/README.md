# Resources
Location of input files and output files from report.

## Input directory
### Directory for files using the ./bin/Report.php function 
./resources/input/report/
### Directory for files using the ./bin/UpdateBarcode.php function
./resources/input/update-barcodes/
### Directory for files using the ./bin/UpdateItem.php function
./resources/input/update-item/

## Output directory
### Directory output files using the ./bin/Report.php function
./resources/output/report/
### Directory output files using the ./bin/UpdateBarcode.php function
./resources/output/update-barcodes/
### Directory output files using the ./bin/UpdateItem.php function
./resources/output/update-item/