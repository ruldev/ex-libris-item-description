<?php

class ItemDescription {
    //class scoped error message
    public $sErrorReason = false;

    //class scoped boolean to stop API request
    public $bAPIThresholdMet = false;

    //default delimiter for multiple enum/chron information
    private $sDefaultDelimiter = ':';

    //default delimiter for year ranges
    private $sDefaultYearRangeDelimiter = '/';

    //default delimiter for multiple enum/chron information
    private $sChronOpener = '(';
    //default delimiter for multiple enum/chron information
    private $sChronCloser = ')';

    //default mapping of enumeration
    private $aEnumerationMap = array('a' => 'v.',
        'b' => 'no.',
        'c' => 'pt.');

    //default mapping of chronology
    private $aChronologyMap = array('01' => 'Jan.',
        '02' => 'Feb.',
        '03' => 'Mar.',
        '04' => 'Apr.',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'Aug.',
        '09' => 'Sept.',
        '10' => 'Oct.',
        '11' => 'Nov.',
        '12' => 'Dec.',
        '21' => 'Spring',
        '22' => 'Summer',
        '23' => 'Fall',
        '24' => 'Winter',
    );

    //exclusion list for parsing evaluation
    private $aParsingExclusionList = array(
        "t." => "tome",
        "r-" => "rand report",
        "p-" => "rand report",
        "n-" => "rand note",
        "ed." => "edition",
        "bd." => "band",
        "abt" => "abteilung",
        "ser." => "series",
        "heft" => "heft",
        "n.f." => "neue folge",
        "leg." => "legislation",
        "unit" => "unit",
        "reel" => "reel",
        "bk." => "book",
        "cong." => "congress",
        "fasc." => "fascicle",
        "pp." => "pages",
        "suppl " => "supplement",
        "suppl." => "supplement",
        "sup." => "supplement",
        "supp." => "supplement",
        "supplement" => "supplement",
        "suppls." => "supplement",
        "n.s." => "new series",
        "n.s.v." => "new series",
        "n.s,v." => "new series",
        "ns.v." => "new series",
        "n.s v." => "new series",
        "h." => "house documents",
        "title" => "title",
        "index" => "index",
        "sess." => "session",
        "sect." => "section",
        "issue" => "issue",
        "spr" => "season",
        "sum" => "season",
        "fall" => "season",
        "win" => "season",
        "s.rep." => "senate report",
        "s. rep." => "senate report",
        "s.doc." => "senate document",
        "s. doc." => "senate document"
    );

    //updateItem header file field requirement
    private $aUpdateHeaderRequiredFields = array(
        "enum a",
        "enum b",
        "enum c",
        "enum d",
        "enum e",
        "enum f",
        "enum g",
        "enum h",
        "chron i",
        "chron j",
        "chron k",
        "chron l",
        "chron m",
        "description",
        "barcode"
    );

    //updateBarcode header file field requirement
    private $aUpdateBarcodeHeaderRequiredFields = array(
        "mmsid",
        "holdingid",
        "itemid",
        "new barcode"
    );

    //updateItem field requirement
    private $aUpdateRequiredFields = array(
        "description",
        "barcode"
    );

    //generateReport field requirement
    private $aGenerateReportRequiredFields = array(
        "description",
        "barcode"
    );

    //loaded configuration
    private $aLoadedConfig = array();


    /**
     * Standard constructor
     *
     */
    function __construct() {
    }//END method construct

    /**
     * Standard destructor
     *
     */
    function __destruct() {
    }//END method destruct

    public function loadConfig($aConfiguration) {
        $this->aLoadedConfig = $aConfiguration;
    }//END method destruct

    /**
     * Provided an array of enumeration and chronology values with named indicies for the enum(a-h) and chron (i-m)
     * return a string of item description following the ANSI/NISO standard for MARC21 holdings.
     *
     * @param       array   aData       Array of data to generate description from
     * @return      mixed               String on success, false(bool) on failure
     */
    public function generateDescription($aData) {
        //empty?
        if (empty($aData)) {
            return null;
        }//END if empty
        else {
            //no values?
            $bValue = false;
            foreach ($aData as $sValue) {
                $bValue = trim($sValue) != '' ? true : $bValue;
            }//END foreach value
            if ($bValue === false) {
                return false;
            }//END if not values
            else {//proceed
                //init
                $sResult = null;

                //Work on enumeration?
                $sEnum = null;
                //Any enumerations?
                $bEnumFound = false;
                foreach ($aData as $sKey => $sValue) {
                    foreach ($this->aEnumerationMap as $sMapKey => $sMapValue) {
                        if ($sKey == $sMapKey && $sValue != ''){
                            $bEnumFound = true;
                            break;
                        }
                    }//END foreach enumeration mapping
                    if ($bEnumFound === true) {
                        break;
                    }
                }//END foreach enumeration
                //continue w/ enumerations?
                if ($bEnumFound == true) {
                    foreach ($this->aEnumerationMap as $sMapKey => $sMapValue) {
                        $sEnum .= isset($aData[$sMapKey]) && $aData[$sMapKey] != "" ? $sMapValue . $aData[$sMapKey] . $this->sDefaultDelimiter : "";
                    }//END foreach enumeration mapping
                    //strip off last delimiter
                    $sEnum = rtrim($sEnum, $this->sDefaultDelimiter);
                }//END if enumerations provided

                //Work on chronology?
                $sChron = null;
                if (isset($aData['i']) && $aData['i'] !='') {
                    //single year or coverage?
                    if (strlen($aData['i']) == 4){
                        //numeric year value?
                        if (!ctype_digit($aData['i'])) {
                            return false;
                        }//END if not numeric
                        else {
                            $sChron = trim($aData['i']);
                        }
                    }//END if length 4
                    else {
                        $mRange = $this->normalizeYear($aData['i']);
                        if ($mRange !== false) {
                            $sChron = $mRange['value'];
                        }
                        else {
                            return false;
                        }
                    }//END else
                }//END if a chron I value was passed
                //month/season?
                if (isset($aData['j']) && $aData['j'] != '') {
                    if ($sChron === null) {
                        return false;
                    }
                    else if (!isset($this->aChronologyMap[$aData['j']])) {
                        return false;
                    }
                    else {
                        $sChron .= $this->sDefaultDelimiter . $this->aChronologyMap[$aData['j']];
                    }
                }//END if a month/season

                //Append results
                //enumeration?
                $sResult = $sEnum !== null ? $sEnum : $sResult;
                //chronology?
                if ($sChron !== null) {
                    $sResult .= $sEnum !== null ? $this->sChronOpener . $sChron . $this->sChronCloser : $sChron;
                }
                return $sResult;
            }//END else processing...

        }//END else not empty
    }//END function generateDescription

    /**
     * Parse legacy string descriptions and provide an array of enum(a-h) and chron (i-m) on success with
     * the matching pattern.
     *
     * @param       string  sData       Data to parse
     * @return      mixed               Array on success, false(bool) on failure
     */
    public function parseDescription($sData){
        //empty/blank/false?
        if ($sData === false || $sData == '' || is_null($sData)) {
            return false;
        }

        //set output
        $aOutput = array('a'=>'', 'b'=>'', 'c'=>'', 'd'=>'', 'e'=>'', 'f'=>'', 'g'=>'', 'h'=>'', 'i'=>'', 'j'=>'', 'k'=>'', 'l'=>'', 'm'=>'', 'pattern' => false);

        $bPatternMatch = false;
        foreach ($this->aParsingExclusionList as $sPatternMatch => $sPattern) {
            if (stripos($sData, " " . $sPatternMatch) !== false) {
                $aOutput['pattern'] = $sPattern;
                $bPatternMatch = true;
                break;
            }//END if
        }//END foreach

        //does it start with volume information?
        if ($bPatternMatch === false) {
            if (strtolower(substr($sData, 0, 2)) == "v.") {
                //trim up and append
                $sParsed = trim(substr($sData, 2));
                if (ctype_digit($sParsed) || (strpos($sParsed, " ") === false && (strpos($sParsed, "/") != false || strpos($sParsed, "-") != false) && ctype_digit(str_replace(array("/", "-"), "", $sParsed))) ) {
                    //ranges?
                    if (strpos($sParsed, "/") != false || strpos($sParsed, "-") != false){
                        //check range is ordered correctly
                        $mRange = $this->parseRange($sParsed);
                        if ($mRange === false) {
                            $aOutput['pattern'] = "";//blank it out
                        }
                        else{
                            $aOutput['pattern'] = "_volume-range";
                            $aOutput['a'] = $mRange;
                        }
                    }//END if range
                    else {
                        //store for output
                        $aOutput['pattern'] = "_volume-only";
                        $aOutput['a'] = $sParsed;
                    }
                }//END if volume
                //go deeper; look for volume and year
                if ($aOutput['a'] == '') {
                    //number and part
                    if ((stripos($sParsed, "pt.") || stripos($sParsed, "p.")) && stripos($sParsed, "no.")) {
                        $sParsed = str_ireplace("p.", "pt.", $sParsed);
                        //replace spaces after indicators first
                        $sParsed = str_ireplace(array("no. ", "pt. ", "no.", "pt."), array("no.", "pt.", "no.", "pt."), $sParsed);
                        //explode on space
                        $aFragments = explode(" ", $sParsed);
                        //no space, try a comma
                        $aFragments = isset($aFragments[0]) && $aFragments[0] == $sParsed ? explode(",", $sParsed) : $aFragments;
                        //no comma, try a colon
                        $aFragments = isset($aFragments[0]) && $aFragments[0] == $sParsed ? explode(":", $sParsed) : $aFragments;
                        //peel off the volume information
                        $iVolume = false;
                        $sPattern = "volume-";
                        if (!stripos($aFragments[0], "no.") && !stripos($aFragments[0], "pt.")) {
                            $iVolume = $this->parseRange(trim($aFragments[0], " ,"));
                            if ($iVolume !== false){
                                unset($aFragments[0]);
                                $sPattern .= is_numeric($iVolume) ? "" : "range-";
                            }
                        }
                        if ($iVolume !== false){
                            //find no. and pt.
                            $iNo = false;
                            $iPt = false;
                            $sYear = false;
                            $mYearFound = false;
                            $mBadDataPart = false;
                            foreach ($aFragments as $sFragment) {
                                $iNo = substr($sFragment, 0, 3) == "no." && $this->parseRange(substr($sFragment, 3)) != false ? substr($sFragment, 3) : $iNo;
                                $iPt = substr($sFragment, 0, 3) == "pt." && $this->parseRange(substr($sFragment, 3)) != false ? substr($sFragment, 3) : $iPt;
                                if (substr($sFragment, 0, 3) != "no." && substr($sFragment, 0, 3) != "pt.") {
                                    $mYearFound = true;
                                    $mYearNormalized = $this->normalizeYear($sFragment);
                                    //year analysis
                                    if ($mYearNormalized !== false) {
                                        $sYear = $mYearNormalized['value'];
                                        $aOutput['pattern'] = "_" . $sPattern . "no-pt-" . $mYearNormalized['pattern'];
                                    }
                                }//END if not part or number information
                                $mBadDataPart = $iNo === false || $iPt === false || $mYearFound === false ? true : $mBadDataPart;
                            }//END foreach
                            //year analysis
                            if ($sYear === false && $mYearFound === true) {
                                $aOutput['pattern'] = $sPattern . "no-pt-year-invalid";
                            }
                            else if ($iNo === false || $iPt === false){
                                $aOutput['pattern'] = "volume-no-pt-year-invalid-data";
                            }
                            else {
                                //store pattern
                                $aOutput['a'] = $iVolume;
                                $aOutput['b'] = $iNo;
                                $aOutput['c'] = $iPt;
                                $aOutput['i'] = $sYear !== false & $mYearFound == true ? $sYear : "";
                                $aOutput['pattern'] = $mYearFound === false ? "_" . $sPattern . "no-pt" : $aOutput['pattern'];
                            }
                        }//END if volume information was found
                        else{
                            $aOutput['pattern'] = $sPattern . "no-pt-invalid-volume";
                        }
                    }//END if volume, number, and part
                    //number or part?
                    else if ((stripos($sParsed, "no.") || stripos($sParsed, "pt.") || stripos($sParsed, "p."))) {
                        $sParsed = str_ireplace("p.", "pt.", $sParsed);
                        //type
                        $mType = stripos($sParsed, "no.") ? "no." : "pt.";
                        $sParsed = str_ireplace(array($mType . " ", $mType), array($mType, $mType), $sParsed);
                        //make sure there is only on instance
                        $sPattern = "volume-";
                        if ((substr_count(strtolower($sParsed), $mType) == 1)) {
                            //split
                            $sParsed = str_ireplace(array($mType." ", $mType), array($mType, $mType), $sParsed);
                            //explode on space
                            $aFragments = explode(" ", $sParsed);
                            //no space, try a comma
                            $aFragments = isset($aFragments[0]) && $aFragments[0] == $sParsed ? explode(",", $sParsed) : $aFragments;
                            //no comma, try a colon
                            $aFragments = isset($aFragments[0]) && $aFragments[0] == $sParsed ? explode(":", $sParsed) : $aFragments;
                            //peel off the volume information
                            $iVolume = false;
                            if (!stripos($aFragments[0], $mType)) {
                                $iVolume = !ctype_digit(trim($aFragments[0], " ,")) ? $this->parseRange(trim($aFragments[0], " ,")) : trim($aFragments[0], " ,");
                                if ($iVolume !== false){
                                    unset($aFragments[0]);
                                    $sPattern .= !ctype_digit($iVolume) ? "range-" : "";
                                }
                            }
                            if ($iVolume !== false){
                                //find no. and pt.
                                $iData = false;
                                $sYear = false;
                                $mYearFound = false;
                                foreach ($aFragments as $sFragment) {
                                    $iData = substr($sFragment, 0, 3) == $mType && $this->parseRange(substr($sFragment, 3)) != false ? substr($sFragment, 3) : $iData;
                                    if (substr($sFragment, 0, 3) != $mType) {
                                        $mYearFound = true;
                                        $mYearNormalized = $this->normalizeYear($sFragment);
                                        //year analysis
                                        if ($mYearNormalized !== false) {
                                            $sYear = $mYearNormalized['value'];
                                            $aOutput['pattern'] = "_" . $sPattern . str_replace(".", "", $mType) . "-" . $mYearNormalized['pattern'];
                                        }
                                    }//END if not a part/number
                                }//END foreach
                                //year analysis
                                if ($sYear === false && $mYearFound === true) {
                                    $aOutput['pattern'] = $sPattern . str_replace(".", "", $mType)  . "-year-invalid";
                                }
                                else if($iData === false)  {
                                    $aOutput['pattern'] = $sPattern . str_replace(".", "", $mType)  . "-invalid";
                                }
                                else {
                                    //store pattern
                                    $aOutput['a'] = $iVolume;
                                    $aOutput['b'] = $mType == "no." ? $iData : $aOutput['b'];
                                    $aOutput['c'] = $mType == "pt." ? $iData : $aOutput['c'];
                                    $aOutput['i'] = $sYear !== false & $mYearFound == true ? $sYear : "";
                                    $aOutput['pattern'] = $mYearFound === false ? "_" . $sPattern . str_replace(".", "", $mType) : $aOutput['pattern'];
                                }
                            }//END if volume information was found
                            else{
                                $aOutput['pattern'] = $sPattern . str_replace(".", "", $mType)  . "-invalid-volume";
                            }
                        }//END if only one pt. or no. is provided
                        else {
                            $aOutput['pattern'] = $sPattern . "unknown";
                        }
                    }//END if contains no. or pt.
                    else {//looking for volume and year matches only
                        $aValParts = explode(" ", $sParsed);
                        if (count($aValParts) > 1) {//merge everything but the first part
                            if (ctype_digit($aValParts[0]) || (strpos($aValParts[0], " ") === false && (strpos($sParsed, "/") != false || strpos($sParsed, "-") != false) && ctype_digit(str_replace(array("/", "-"), "", $aValParts[0]))) ) {
                                $sPartOne = $aValParts[0];
                                unset($aValParts[0]);
                                $sRemain = trim(implode(" ", $aValParts));
                                if ((strlen($sRemain) == 4 && ctype_digit($sRemain)) || ((strpos($sParsed, "/") != false || strpos($sParsed, "-") != false) && ctype_digit(str_replace(array("/", "-"), "", $sRemain))) ) {
                                    //store for output
                                    $aOutput['a'] = $sPartOne;
                                    //year okay?
                                    $mRange = $this->normalizeYear($sRemain);
                                    if ($mRange === false) {
                                        $aOutput['a'] = "";//blank it out
                                        $aOutput['pattern'] = "volume-and-year-invalid";//blank it out
                                    }
                                    else {
                                        $aOutput['pattern'] = "_volume-and-" . $mRange['pattern'];
                                        $aOutput['i'] = $mRange['value'];
                                    }
                                }//END if a single year or a range divided by a forward slash
                            }//END if part one is numeric, a volume number
                        }//END if a space is present
                    }//END else
                }//END if not a volume only
            }//END if
            else if (strtolower(substr($sData, 0, 3)) == "yr." || strtolower(substr($sData, 0, 3)) == "yr ") {
                $aOutput['pattern'] = "year";
                $sTrimmed = trim(substr($sData, 3));
                //trim up and append
                $mYear = ctype_digit($sTrimmed) ? $sTrimmed : false;
                $mYearNormalized = $this->normalizeYear($sTrimmed);
                if (is_array($mYearNormalized)) {
                    $aOutput['i'] = $mYearNormalized['value'];
                    $aOutput['pattern'] = "_" . $mYearNormalized['pattern'];
                }
                //vol, no, and pt
                else if (strlen($sTrimmed) > 4 && ctype_digit(substr($sTrimmed, 0, 4)) && stripos($sTrimmed, "v.") && stripos($sTrimmed, "no.") && (stripos($sTrimmed, "pt.") !== false || stripos($sTrimmed, "p.") !== false)) {
                    $sTrimmed = str_ireplace("p.", "pt.", $sTrimmed);
                    //squish the v. and pt. together with the value
                    $sTrimmed = str_ireplace(array("v. ", "no. ", "pt. "), array("v.", "no.", "pt."), $sTrimmed);
                    //explode on space
                    $aFragments = explode(" ", $sTrimmed);
                    //no space, try a comma
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(",", $sTrimmed) : $aFragments;
                    //no comma, try a colon
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(":", $sTrimmed) : $aFragments;
                    //skip if not at least 3 parts
                    if (count($aFragments) <= 3) {
                        $aOutput['pattern'] = "year-vol-and-no-and-part-invalid-delimiter";
                    }
                    else {
                        $sPattern = "year-vol-and-no-and-part";
                        //partial or complete range?
                        $mYearNormalized = $this->normalizeYear(trim($aFragments[0]));
                        if ($mYearNormalized === false){
                            $aOutput['pattern'] = $sPattern . "-invalid-year";
                        }
                        else {
                            $aOutput['i'] = $mYearNormalized['value'];
                            //figure out volume and part
                            foreach ($aFragments as $sFragment) {
                                //set type of the value we are parsing
                                $eType = stripos($sFragment, "no.") !== false ? "no." : stripos($sFragment, "v.");
                                $eType = is_numeric($eType)? "v." : $eType;
                                $eType = $eType === false && is_numeric(stripos($sFragment, "pt.")) ? "pt." : $eType;
                                //strip out the indicator
                                $mVal = str_ireplace($eType, "", $sFragment);
                                //parse and test
                                $mVal = !ctype_digit($mVal) ? $this->parseRange(trim($mVal)) : $mVal;
                                //set
                                switch ($eType) {
                                    case "v.":
                                        $aOutput['a'] = $mVal;
                                        break;
                                    case "no.":
                                        $aOutput['b'] = $mVal;
                                        break;
                                    case "pt.":
                                        $aOutput['c'] = $mVal;
                                        break;
                                }
                            }//END foreach
                            $aOutput['pattern'] = $aOutput['a'] != "" && $aOutput['b'] != "" && $aOutput['c'] != "" ? "_" . $sPattern . "-" . $mYearNormalized['pattern'] : $sPattern . "-" . $mYearNormalized['pattern'];
                            $aOutput['pattern'] = $aOutput['a'] == "" ? $aOutput['pattern'] . "-invalid-vol" : $aOutput['pattern'];
                            $aOutput['pattern'] = $aOutput['b'] == "" ? $aOutput['pattern'] . "-invalid-no" : $aOutput['pattern'];
                            $aOutput['pattern'] = $aOutput['c'] == "" ? $aOutput['pattern'] . "-invalid-part" : $aOutput['pattern'];
                        }//END else, valid year
                    }//END else, data points found
                }//END if vol, no, pt
                //vol and pt
                else if (strlen($sTrimmed) > 4 && ctype_digit(substr($sTrimmed, 0, 4)) && stripos($sTrimmed, "v.") && (stripos($sTrimmed, "pt.") !== false || stripos($sTrimmed, "p.") !== false)) {
                    $sTrimmed = str_ireplace("p.", "pt.", $sTrimmed);
                    //squish the v. and pt. together with the value
                    $sTrimmed = str_ireplace(array("v. ", "pt. "), array("v.", "pt."), $sTrimmed);
                    //explode on space
                    $aFragments = explode(" ", $sTrimmed);
                    //no space, try a comma
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(",", $sTrimmed) : $aFragments;
                    //no comma, try a colon
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(":", $sTrimmed) : $aFragments;
                    //skip if not at least 3 parts
                    if (count($aFragments) <= 2) {
                        $aOutput['pattern'] = "year-vol-and-part-invalid-delimiter";
                    }
                    else {
                        $sPattern = "year-vol-and-part";
                        //partial or complete range?
                        $mYearNormalized = $this->normalizeYear(trim($aFragments[0]));
                        if ($mYearNormalized === false){
                            $aOutput['pattern'] = $sPattern . "-invalid-year";
                        }
                        else {
                            $aOutput['i'] = $mYearNormalized['value'];
                            //figure out volume and part
                            foreach ($aFragments as $sFragment) {
                                //set type of the value we are parsing
                                $eType = stripos($sFragment, "pt.") !== false ? "pt." : stripos($sFragment, "v.");
                                $eType = is_numeric($eType)? "v." : $eType;
                                //strip out the indicator
                                $mVal = str_ireplace($eType, "", $sFragment);
                                //prase and test
                                $mVal = !ctype_digit($mVal) ? $this->parseRange(trim($mVal)) : $mVal;
                                //set
                                switch ($eType) {
                                    case "v.":
                                        $aOutput['a'] = $mVal;
                                        break;
                                    case "pt.":
                                        $aOutput['c'] = $mVal;
                                        break;
                                }
                            }//END foreach
                            $aOutput['pattern'] = $aOutput['a'] != "" && $aOutput['c'] != "" ? "_" . $sPattern . "-" . $mYearNormalized['pattern'] : $sPattern . "-" . $mYearNormalized['pattern'];
                            $aOutput['pattern'] = $aOutput['a'] == "" ? $aOutput['pattern'] . "-invalid-vol" : $aOutput['pattern'];
                            $aOutput['pattern'] = $aOutput['c'] == "" ? $aOutput['pattern'] . "-invalid-part" : $aOutput['pattern'];
                        }//END else, valid year
                    }//END else, data points found
                }//END if vol and part
                //vol and num
                else if (strlen($sTrimmed) > 4 && ctype_digit(substr($sTrimmed, 0, 4)) && stripos($sTrimmed, "v.") && stripos($sTrimmed, "no.")) {
                    //squish the v. and no. together with the value
                    $sTrimmed = str_ireplace(array("v. ", "no. "), array("v.", "no."), $sTrimmed);
                    //explode on space
                    $aFragments = explode(" ", $sTrimmed);
                    //no space, try a comma
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(",", $sTrimmed) : $aFragments;
                    //no comma, try a colon
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(":", $sTrimmed) : $aFragments;
                    //skip if not at least 3 parts
                    if (count($aFragments) <= 2) {
                        $aOutput['pattern'] = "year-vol-and-no-invalid-delimiter";
                    }
                    else {
                        $sPattern = "year-vol-and-no";
                        //partial or complete range?
                        $mYearNormalized = $this->normalizeYear(trim($aFragments[0]));
                        if ($mYearNormalized === false){
                            $aOutput['pattern'] = $sPattern . "-invalid-year";
                        }
                        else {
                            $aOutput['i'] = $mYearNormalized['value'];
                            //figure out volume and part
                            foreach ($aFragments as $sFragment) {
                                //set type of the value we are parsing
                                $eType = stripos($sFragment, "no.") !== false ? "no." : stripos($sFragment, "v.");
                                $eType = is_numeric($eType)? "v." : $eType;
                                //strip out the indicator
                                $mVal = str_ireplace($eType, "", $sFragment);
                                //parse and test
                                $mVal = !is_numeric($mVal) ? $this->parseRange(trim($mVal)) : $mVal;
                                //set
                                switch ($eType) {
                                    case "v.":
                                        $aOutput['a'] = $mVal;
                                        break;
                                    case "no.":
                                        $aOutput['b'] = $mVal;
                                        break;
                                }
                            }//END foreach
                            $aOutput['pattern'] = $aOutput['a'] != "" && $aOutput['b'] != "" ? "_" . $sPattern . "-" . $mYearNormalized['pattern'] : $sPattern . "-" . $mYearNormalized['pattern'];
                            if ($aOutput['a'] == "" || $aOutput['b'] == "") {
                                $aOutput['a'] = $aOutput['b'] = $aOutput['i'] = "";
                                $aOutput['pattern'] = $aOutput['a'] == "" ? $aOutput['pattern'] . "-invalid-vol" : $aOutput['pattern'];
                                $aOutput['pattern'] = $aOutput['b'] == "" ? $aOutput['pattern'] . "-invalid-no" : $aOutput['pattern'];
                            }
                        }//END else, valid year
                    }//END else, data points found
                }//END if vol and num
                //no and pt
                else if (strlen($sTrimmed) > 4 && ctype_digit(substr($sTrimmed, 0, 4)) && stripos($sTrimmed, "no.") && (stripos($sTrimmed, "pt.") !== false  || stripos($sTrimmed, "p.") !== false)) {
                    $sTrimmed = str_ireplace("p.", "pt.", $sTrimmed);
                    //squish the no. and pt. together with the value
                    $sTrimmed = str_ireplace(array("no. ", "pt. "), array("no.", "pt."), $sTrimmed);
                    //explode on space
                    $aFragments = explode(" ", $sTrimmed);
                    //no space, try a comma
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(",", $sTrimmed) : $aFragments;
                    //no comma, try a colon
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(":", $sTrimmed) : $aFragments;
                    //skip if not at least 3 parts
                    if (count($aFragments) <= 2) {
                        $aOutput['pattern'] = "year-no-and-part-invalid-delimiter";
                    }
                    else {
                        $sPattern = "year-no-and-part";
                        //partial or complete range?
                        $mYearNormalized = $this->normalizeYear(trim($aFragments[0]));
                        if ($mYearNormalized === false){
                            $aOutput['pattern'] = $sPattern . "-invalid-year";
                        }
                        else {
                            $aOutput['i'] = $mYearNormalized['value'];
                            //figure out volume and part
                            foreach ($aFragments as $sFragment) {
                                //set type of the value we are parsing
                                $eType = stripos($sFragment, "pt.") !== false ? "pt." : stripos($sFragment, "no.");
                                $eType = is_numeric($eType)? "no." : $eType;
                                //strip out the indicator
                                $mVal = str_ireplace($eType, "", $sFragment);
                                //prase and test
                                $mVal = !ctype_digit($mVal) ? $this->parseRange(trim($mVal)) : $mVal;
                                //set
                                switch ($eType) {
                                    case "no.":
                                        $aOutput['b'] = $mVal;
                                        break;
                                    case "pt.":
                                        $aOutput['c'] = $mVal;
                                        break;
                                }
                            }//END foreach
                            $aOutput['pattern'] = $aOutput['b'] != "" && $aOutput['c'] != "" ? "_" . $sPattern . "-" . $mYearNormalized['pattern'] : $sPattern . "-" . $mYearNormalized['pattern'];
                            $aOutput['pattern'] = $aOutput['b'] == "" ? $aOutput['pattern'] . "-invalid-no" : $aOutput['pattern'];
                            $aOutput['pattern'] = $aOutput['c'] == "" ? $aOutput['pattern'] . "-invalid-part" : $aOutput['pattern'];
                        }//END else, valid year
                    }//END else, data points found
                }//END if number and part
                //only pt
                else if (strlen($sTrimmed) > 4 && ctype_digit(substr($sTrimmed, 0, 4)) && (stripos($sTrimmed, "pt.") !== false || stripos($sTrimmed, "p.") !== false)) {
                    $sTrimmed = str_ireplace("p.", "pt.", $sTrimmed);
                    //squish the pt. together with the value
                    $sTrimmed = str_ireplace("pt. ", "pt.", $sTrimmed);
                    //explode on space
                    $aFragments = explode(" ", $sTrimmed);
                    //no space, try a comma
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(",", $sTrimmed) : $aFragments;
                    //no comma, try a colon
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(":", $sTrimmed) : $aFragments;
                    //skip if not at least 2 parts
                    if (count($aFragments) <= 1) {
                        $aOutput['pattern'] = "year-range-and-part-invalid-delimiter";
                    }
                    else {
                        //partial or complete range?
                        $mYearNormalized = $this->normalizeYear(trim($aFragments[0]));
                        if ($mYearNormalized === false){
                            $aOutput['pattern'] = "year-range-and-part-invalid-year";
                        }
                        else {

                            //figure out volume
                            $mPart = isset($aFragments[1]) ? str_ireplace("pt.", "", $aFragments[1]) : false;
                            $mPart = !ctype_digit($mPart) ? $this->parseRange(trim($mPart)) : $mPart;
                            if ($mPart !== false) {//okay?
                                $aOutput['c'] = $mPart;
                                $aOutput['i'] = $mYearNormalized['value'];
                                $aOutput['pattern'] = "_" . $mYearNormalized['pattern'] . "-and-part";
                            }
                            else {
                                $aOutput['pattern'] = $mYearNormalized['pattern'] . "-and-part-invalid-part";
                            }
                        }//END else, valid year
                    }//END else, data points found
                }//END else if part information included
                //only vol
                else if (strlen($sTrimmed) > 4 && ctype_digit(substr($sTrimmed, 0, 4)) && stripos($sTrimmed, "v.")) {
                    //squish the v. together with the value
                    $sTrimmed = str_ireplace("v. ", "v.", $sTrimmed);
                    //explode on space
                    $aFragments = explode(" ", $sTrimmed);
                    //no space, try a comma
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(",", $sTrimmed) : $aFragments;
                    //no comma, try a colon
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(":", $sTrimmed) : $aFragments;
                    //skip if not at least 2 parts
                    if (count($aFragments) <= 1) {
                        $aOutput['pattern'] = "year-range-and-volume-invalid-delimiter";
                    }
                    else {
                        //partial or complete range?
                        $mYearNormalized = $this->normalizeYear(trim($aFragments[0]));
                        if ($mYearNormalized === false){
                            $aOutput['pattern'] = "year-range-and-volume-invalid-year";
                        }
                        else {
                            //figure out volume
                            $mVolume = isset($aFragments[1]) ? str_ireplace("v.", "", $aFragments[1]) : false;
                            $mVolume = !ctype_digit($mVolume) ? $this->parseRange(trim($mVolume)) : $mVolume;
                            if ($mVolume !== false) {//okay?
                                $aOutput['a'] = $mVolume;
                                $aOutput['i'] = $mYearNormalized['value'];
                                $aOutput['pattern'] = "_" . $mYearNormalized['pattern'] . "-and-volume";
                            }
                            else {
                                $aOutput['pattern'] = $mYearNormalized['pattern'] . "-and-volume-invalid-volume";
                            }
                        }//END else, valid year
                    }//END else, data points found
                }//END else if volume information included
                //only no
                else if (stripos($sTrimmed, "no.")) {//number included?
                    //squish the no. together with the value
                    $sTrimmed = str_ireplace("no. ", "no.", $sTrimmed);
                    //explode on space
                    $aFragments = explode(" ", $sTrimmed);
                    //no space, try a comma
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(",", $sTrimmed) : $aFragments;
                    //no comma, try a colon
                    $aFragments = isset($aFragments[0]) && $aFragments[0] == $sTrimmed ? explode(":", $sTrimmed) : $aFragments;
                    //skip if not at least 2 parts
                    if (count($aFragments) <= 1) {
                        $aOutput['pattern'] = "year-range-and-number-invalid-delimiter";
                    }
                    else {
                        //partial or complete range?
                        $mYearNormalized = $this->normalizeYear($aFragments[0]);
                        if ($mYearNormalized === false){
                            $aOutput['pattern'] = "year-range-and-number-invalid-year";
                        }
                        else {
                            //figure out volume
                            $mNumber = isset($aFragments[1]) ? str_ireplace("no.", "", $aFragments[1]) : false;
                            $mNumber = !ctype_digit($mNumber) ? $this->parseRange(trim($mNumber)) : $mNumber;
                            if ($mNumber !== false) {//okay?
                                $aOutput['b'] = $mNumber;
                                $aOutput['i'] = $mYearNormalized['value'];
                                $aOutput['pattern'] = "_" . $mYearNormalized['pattern'] . "-and-number";
                            }
                            else {
                                $aOutput['pattern'] = $mYearNormalized['pattern'] . "-and-number-invalid-number";
                            }
                        }//END else, valid year
                    }//END else, data points found
                }//END else if a number is included
            }//END if
            else if (strlen($sData) == 4 && ctype_digit($sData)) {
                $aOutput['pattern'] = "_year-only";
                $aOutput['i'] = $sData;
            }//END if
            else if (strtolower(substr($sData, 0, 3)) == "no.") {
                $aOutput['pattern'] = "number";
                //trim up and append
                $sParsed = trim(substr($sData, 3));
                if (ctype_digit($sParsed) || (strpos($sParsed, " ") === false && strpos($sParsed, "/") != false && ctype_digit(str_replace("/", "", $sParsed))) ) {
                    $aOutput['b'] = $sParsed;
                    $aOutput['pattern'] = "_number-only";
                }
                else {
                    $aValParts = explode(" ", $sParsed);
                    if (count($aValParts) > 1) {//merge everything but the first part
                        if (ctype_digit($aValParts[0]) || (strpos($aValParts[0], " ") === false && strpos($aValParts[0], "/") != false && ctype_digit(str_replace("/", "", $aValParts[0]))) ) {
                            $sPartOne = $aValParts[0];
                            unset($aValParts[0]);
                            $sRemain = trim(implode(" ", $aValParts));
                            if ((strlen($sRemain) == 4 && ctype_digit($sRemain)) || (strpos($sRemain, "/") != false && ctype_digit(str_replace("/", "", $sRemain))) ) {
                                $aOutput['b'] = $sPartOne;
                                //parse year
                                $mYearNormalized = $this->normalizeYear($sRemain);
                                //store for output
                                if (is_array($mYearNormalized) && isset($mYearNormalized['value'], $mYearNormalized['pattern'])) {
                                    $aOutput['i'] = $mYearNormalized['value'];
                                    $aOutput['pattern'] = "_number-and-" . $mYearNormalized['pattern'];
                                }
                                else {//bad year value
                                    $aOutput['b'] = "";//clear
                                    $aOutput['pattern'] = "number-and-year-invalid";
                                }
                            }//END if a single year or a range divided by a forward slash
                        }//END if part one is numeric, a volume number
                    }//END if a space is present
                }//END else
            }//END if
            else if (ctype_digit(substr($sData, 0, 4)) && isset($sData[4]) && ($sData[4] == "-" || $sData[4] == "/" || stripos($sData, "no .") || stripos($sData, "no.") || stripos($sData, "v.") || stripos($sData, "v ."))) {
                $aOutput['pattern'] = "year-four-with-part";
            }//END if
            else if (strlen($sData) == 3 && ctype_digit($sData)) {
                $aOutput['pattern'] = "sudoc-only-two";
            }//END if
            else if (ctype_digit(substr($sData, 0, 3)) && isset($sData[3]) && ($sData[3] == "-" || $sData[3] == "/")) {
                $aOutput['pattern'] = "sudoc-three-with-part";
            }//END if
            else if (strlen($sData) == 2 && ctype_digit($sData)) {
                $aOutput['pattern'] = "sudoc-only-two";
            }//END if
            else if (ctype_digit(substr($sData, 0, 2)) && isset($sData[2]) && ($sData[2] == "-" || $sData[2] == "/")) {
                $aOutput['pattern'] = "sudoc-only-two-with-part";
            }//END if
            else if (strlen($sData) == 1 && ctype_digit($sData)) {
                $aOutput['pattern'] = "sudoc-only-one";
            }//END if
            else if (ctype_digit(substr($sData, 0, 1)) && isset($sData[1]) && ($sData[1] == "-" || $sData[1] == "/")) {
                $aOutput['pattern'] = "sudoc-only-one-with-part";
            }//END if
            else {
                $bPatternMatch = false;
                foreach ($this->aParsingExclusionList as $sPatternMatch => $sPattern) {
                    if (strtolower(substr($sData, 0, strlen($sPatternMatch))) == $sPatternMatch) {
                        $aOutput['pattern'] = $sPattern;
                        $bPatternMatch = true;
                        break;
                    }//END if
                }//END foreach
                //set to unknown otherwise
                $aOutput['pattern'] = $bPatternMatch === false ? "unknown" : $aOutput['pattern'];
            }//END else
        }//END if no stripos pattern match
        //return
        return $aOutput;
    }//END function parseDescription

    /**
     * Parse a range and return if valid
     *
     * @param       string  sRange      Range to parse
     * @return      mixed               String of range on success, false(bool) on failure
     */
    public function parseRange($sRange, $mType = false) {
        if (strpos($sRange, "/") != false || strpos($sRange, "-") != false){
            //check range is ordered correctly
            $aParts = strpos($sRange, "/") != false ? explode("/", $sRange) : explode("-", $sRange);
            if (count($aParts) == 2 && ctype_digit($aParts[0]) && ctype_digit($aParts[1]) && (strlen($aParts[0]) <= strlen($aParts[1]))) {
                //second number greater than the first
                if ($aParts[0] < $aParts[1]) {
                    return $sRange;
                }
                //partial second string
                else if (strlen($aParts[0]) > strlen($aParts[1])) {
                    $sStartStripped = substr($aParts[0], 0, (strlen($aParts[0]) - strlen($aParts[1])));
                    $sStartRemained = substr($aParts[0], (strlen($aParts[0]) - strlen($aParts[1])));
                    if ($sStartRemained < $aParts[1]){
                        return substr($sRange, 0, -strlen($aParts[1])) . $sStartStripped . $aParts[1];
                    }
                    else {
                        return false;
                    }
                }//END else if
                else {//otherwise...
                    return false;
                }
            }//END if two parts
            else {
                return false;
            }
        }//END if range
        else if(ctype_digit($sRange)){//just a single entry
            return $sRange;
        }
        else {
            return false;
        }
    }//END function parseRange



    /** Normalize a year string value, providing a newly normalized value.  False on error.
     *
     * @param       string    sYear     Year to parse
     * @return      mixed               FALSE w/ parsing error, array with value and pattern keys
     */
    public function normalizeYear($sYear) {
        //cleanup up value
        $sYear = trim($sYear, " ,;:/");
        //simple year?
        if (strlen($sYear) == 4 && ctype_digit($sYear)) {
            return array('value' => $sYear, 'pattern' => 'year-only');
        }
        //range, full years?
        else if (strlen($sYear) == 9 && ctype_digit(substr($sYear, 0, 4)) && ctype_digit(substr($sYear, 5, 4)) && (substr($sYear, 4, 1) == "/" || substr($sYear, 4, 1) == "-")) {
            $sThisDelimiter = is_numeric(stripos($sYear, "/")) ? "/" : "-";
            //same start and end year?
            if (substr($sYear, 0, 4) == substr($sYear, 5, 4)) {
                return array('value' => substr($sYear, 0, 4), 'pattern' => 'year-only');
            }
            //end year is after start year?
            else if (substr($sYear, 0, 4) < substr($sYear, 5, 4)){
                return array('value' => substr($sYear, 0, 4) . $sThisDelimiter . substr($sYear, 5, 4), 'pattern' => 'year-range');
            }
            //end year is before start year
            else {
                return false;
            }
        }//END else if is a full year range
        //range, partial end year
        else if (strlen($sYear) == 7 && ctype_digit(substr($sYear, 0, 4)) && ctype_digit(substr($sYear, 5, 2)) && (substr($sYear, 4, 1) == "/" || substr($sYear, 4, 1) == "-")) {
            $sThisDelimiter = is_numeric(stripos($sYear, "/")) ? "/" : "-";
            //calculate end year
            $sStartYearMCMarker = substr($sYear, 0, 2);
            //is end partial range, with millenium/century(MC) marker after start year?
            if (substr($sYear, 0, 4) < $sStartYearMCMarker . substr($sYear, 5, 2)){
                return array('value' => substr($sYear, 0, 4) . $sThisDelimiter . $sStartYearMCMarker . substr($sYear, 5, 2), 'pattern' => 'year-range');
            }
            //else, tick up the century and see if that works
            else if (substr($sYear, 0, 4) < (($sStartYearMCMarker+1) . substr($sYear, 5, 2))){
                return array('value' => substr($sYear, 0, 4) . $sThisDelimiter . ($sStartYearMCMarker+1) . substr($sYear, 5, 2), 'pattern' => 'year-range');
            }
            else {//some other error
                return false;
            }
        }//END else if is a full year range with partial end year
        //failsafe
        return false;
    }//END function normalizeYear

    /** Generate report
     *
     * @param       resource    mFile   File to process
     * @param       bool        bEcho   true to echo status, false for silent
     * @return      string               String of results
     */
    public function generateReport($mFile, $mEcho = true) {
        //file exists?
        if (!file_exists($mFile)) {
            exit("File not found.\nExiting....\n");
        }

        //continue, load lines
        $aLines = @file($mFile);

        //check headers exist in first line of the file
        $mValidationResult = $this->headerValidationAndMapping($aLines[0], $this->aGenerateReportRequiredFields);
        if ($mValidationResult === false) {
            return false;
        }
        else {
            $aHeaderMapping = $mValidationResult;
        }
        //remove header from file
        unset($aLines[0]);

        if ($mEcho == true) {
            echo "Found " . count($aLines) . " lines in the file.  Performing pattern recognition...\n";
        }
        //set output header
        $sDelimitedOutput = "Enum A|Enum B|Enum C|Enum D|Enum E|Enum F|Enum G|Enum H|Chron I|Chron J|Chron K|Chron L|Chron M|Input Desc|Output Desc|Desc Diff?|Barcode|Match Pattern\n";
        //loop and process
        foreach ($aLines as $aLine) {
            $aParts = explode("|", trim($aLine));
            $sBarcode = trim($aParts[$aHeaderMapping['barcode']]);
            $sInputDesc = trim($aParts[$aHeaderMapping['description']]);
            $mParsedDesc = $this->parseDescription($sInputDesc);
            if ($mParsedDesc === false) {
                $sDelimitedOutput .= "|||||||||||||" . $sInputDesc . "|||" .  $sBarcode . "|";
            }
            else {
                $sDelimitedOutput .= $mParsedDesc['a'] . "|";
                $sDelimitedOutput .= $mParsedDesc['b'] . "|";
                $sDelimitedOutput .= $mParsedDesc['c'] . "|";
                $sDelimitedOutput .= $mParsedDesc['d'] . "|";
                $sDelimitedOutput .= $mParsedDesc['e'] . "|";
                $sDelimitedOutput .= $mParsedDesc['f'] . "|";
                $sDelimitedOutput .= $mParsedDesc['g'] . "|";
                $sDelimitedOutput .= $mParsedDesc['h'] . "|";
                $sDelimitedOutput .= $mParsedDesc['i'] . "|";
                $sDelimitedOutput .= $mParsedDesc['j'] . "|";
                $sDelimitedOutput .= $mParsedDesc['k'] . "|";
                $sDelimitedOutput .= $mParsedDesc['l'] . "|";
                $sDelimitedOutput .= $mParsedDesc['m'] . "|";
                $sDelimitedOutput .= $sInputDesc . "|";
                $mOutputDesc = $this->generateDescription($mParsedDesc);
                $sDelimitedOutput .= !is_null($mOutputDesc) && $mOutputDesc !== false ? $mOutputDesc . "|" : "|";
                $sDelimitedOutput .= $sInputDesc != $mOutputDesc ? "true|" : "false|";
                $sDelimitedOutput .= $sBarcode . "|" . $mParsedDesc['pattern'];
            }//END else
            $sDelimitedOutput .= "\n";
        }//END foreach line
        return $sDelimitedOutput;
    }//END function generateReport

    /** Generate report
     *
     * @param       resource    mFile   File to process
     * @param       string      eMode   'commit' to write change, 'report' for report mode, no updates in report mode
     * @param       bool        bEcho   true to echo status, false for silent
     * @return      mixed               false on error, array with 'results' indices and 'errors' indices
     */
    public function updateItems($mFile, $eMode, $mEcho = true) {
        //file exists?
        if (!file_exists($mFile)) {
            $this->sErrorReason = "Input file was not found";
            return false;
        }
        //load lines
        $aLines = @file($mFile);
        //any lines?
        if (!is_array($aLines) || empty($aLines)) {
            $this->sErrorReason = "Input file is empty";
            return false;
        }

        //check headers exist in first line of the file
        $mInputHeader = explode("|", $aLines[0]);
        $mValidationResult = $this->headerValidationAndMapping($aLines[0], $this->aUpdateHeaderRequiredFields);
        if ($mValidationResult === false) {
            return false;
        }
        else {
            $aHeaderMapping = $mValidationResult;
        }
        //remove header from file
        unset($aLines[0]);

        //continue
        if ($mEcho == true) {//print initial summary
            echo "Found " . count($aLines) . " lines in the file.\n";
        }

        //set output header
        $sOutput = trim(implode("|", $mInputHeader)) . "|mode|note\n";

        //set error header
        $sErrorOutput = "Line #|Barcode|Error\n";

        //process
        $iLine = 2;//starting at two because of the header in the input file
        foreach ($aLines as $sLine) {
            //status message
            if ($mEcho == true){
                echo "Working on " . ($iLine-1) . " of " . count($aLines) . " (" . round((($iLine-1) / (count($aLines) / 100)), 2) . "%)   \r";
            }
            //explode
            $aLineParts = explode("|", trim($sLine));
            $bRequirementMet = true;
            //Check that required fields are provided with values
            foreach ($this->aUpdateRequiredFields as $sRequiredField){
                $bRequirementMet = $bRequirementMet !== false && $aLineParts[$aHeaderMapping[strtolower($sRequiredField)]] != "" ? $bRequirementMet : false;
            }//END foreach
            //required field with values not provided
            if ($bRequirementMet === false) {
                $sErrorBarcodeID = isset($aLineParts[$aHeaderMapping['barcode']]) ? $aLineParts[$aHeaderMapping['barcode']] : "no barcode";
                $sErrorOutput .= $iLine . "|" . $sErrorBarcodeID . "|Required field with no value\n";
            }
            else {
                //continue, lookup item information
                $mResponse = $this->getItem("barcode", array("barcode" => $aLineParts[$aHeaderMapping['barcode']]));
                if ($mResponse === false) {
                    $sErrorOutput .= $iLine . "|" . $aLineParts[$aHeaderMapping['barcode']] ."|Error getting barcode " . $this->sErrorReason . "\n";
                }
                else {
                    //set new values
                    $mResponse->item_data->enumeration_a = $aLineParts[$aHeaderMapping['enum a']];
                    $mResponse->item_data->enumeration_b = $aLineParts[$aHeaderMapping['enum b']];
                    $mResponse->item_data->enumeration_c = $aLineParts[$aHeaderMapping['enum c']];
                    $mResponse->item_data->enumeration_d = $aLineParts[$aHeaderMapping['enum d']];
                    $mResponse->item_data->enumeration_e = $aLineParts[$aHeaderMapping['enum e']];
                    $mResponse->item_data->enumeration_f = $aLineParts[$aHeaderMapping['enum f']];
                    $mResponse->item_data->enumeration_g = $aLineParts[$aHeaderMapping['enum g']];
                    $mResponse->item_data->enumeration_h = $aLineParts[$aHeaderMapping['enum h']];
                    $mResponse->item_data->chronology_i = $aLineParts[$aHeaderMapping['chron i']];
                    $mResponse->item_data->chronology_j = $aLineParts[$aHeaderMapping['chron j']];
                    $mResponse->item_data->chronology_k = $aLineParts[$aHeaderMapping['chron k']];
                    $mResponse->item_data->chronology_l = $aLineParts[$aHeaderMapping['chron l']];
                    $mResponse->item_data->chronology_m = $aLineParts[$aHeaderMapping['chron m']];
                    //set new description
                    $mResponse->item_data->description = $aLineParts[$aHeaderMapping['description']];
                    //start output log response
                    $sOutput .= implode("|", $aLineParts) . "|" . $eMode . "|";
                    //mode switch
                    switch ($eMode) {
                        case "commit":
                            $mResult = $this->updateItem($mResponse);
                            if ($mResult === false) {
                                $sErrorOutput .= $iLine . "|" . $aLineParts[$aHeaderMapping['barcode']] . "|Error updating->" . $this->sErrorReason . "\n";
                                $sOutput .= "error updating\n";
                            }
                            else {
                                $sOutput .= "success\n";
                            }
                            break;
                        default://report
                            $sOutput .= "not updated\n";
                    }//END switch
                }//END else, barcode request successful
            }//END else, requirement values provided
            $iLine++;
        }//END foreach
        return array("results" => $sOutput, "errors" => $sErrorOutput);
    }//END function updateItem

    /** Update a set of barcodes
     *
     * @param       resource    mFile   File to process
     * @param       string      eMode   'commit' to write change, 'report' for report mode, no updates in report mode
     * @param       bool        bEcho   true to echo status, false for silent
     * @return      mixed               false on error, array with 'results' indices and 'errors' indices
     */
    public function updateBarcodes($mFile, $eMode, $mEcho = true) {
        //file exists?
        if (!file_exists($mFile)) {
            $this->sErrorReason = "Input file was not found";
            return false;
        }
        //load lines
        $aLines = @file($mFile);
        //any lines?
        if (!is_array($aLines) || empty($aLines)) {
            $this->sErrorReason = "Input file is empty";
            return false;
        }

        //check headers exist in first line of the file
        $mInputHeader = explode("|", $aLines[0]);
        $mValidationResult = $this->headerValidationAndMapping($aLines[0], $this->aUpdateBarcodeHeaderRequiredFields);
        if ($mValidationResult === false) {
            return false;
        }
        else {
            $aHeaderMapping = $mValidationResult;
        }
        //remove header from file
        unset($aLines[0]);

        //continue
        if ($mEcho == true) {//print initial summary
            echo "Found " . count($aLines) . " lines in the file.\n";
        }

        //set output header
        $sOutput = trim(implode("|", $mInputHeader)) . "|mode|note\n";
        //set error header
        $sErrorOutput = "Line #|Error\n";

        //process
        $iLine = 2;//starting at two because of the header in the input file
        foreach ($aLines as $sLine) {
            //status message
            if ($mEcho == true){
                echo "Working on " . ($iLine-1) . " of " . count($aLines) . " (" . round((($iLine-1) / (count($aLines) / 100)), 2) . "%)   \r";
            }
            //split the line
            $aLineParts = explode("|", trim($sLine));
            //continue, lookup item information
            $aLookupValues = array(
                    "mms_id" => $aLineParts[$aHeaderMapping['mmsid']],
                    "holding_id" => $aLineParts[$aHeaderMapping['holdingid']],
                    "item_id" => $aLineParts[$aHeaderMapping['itemid']]);
            $mResponse = $this->getItem("itemid", $aLookupValues);
            if ($mResponse === false) {
                $sErrorOutput .= $iLine . "|Error getting barcode " . $this->sErrorReason . "\n";
            }
            else {
                //set new description
                $mResponse->item_data->barcode = $aLineParts[$aHeaderMapping['new barcode']];
                //start output log response
                $sOutput .= implode("|", $aLineParts) . "|" . $eMode . "|";
                //mode switch
                switch ($eMode) {
                    case "commit":
                        $mResult = $this->updateItem($mResponse);
                        if ($mResult === false) {
                            $sErrorOutput .= $iLine . "|Error updating->" . $this->sErrorReason . "\n";
                            $sOutput .= "error updating\n";
                        }
                        else {
                            $sOutput .= "success\n";
                        }
                        break;
                    default://report
                        $sOutput .= "not updated\n";
                }//END switch
            }//END else, barcode request successful
            $iLine++;
        }//END foreach
        return array("results" => $sOutput, "errors" => $sErrorOutput);
    }//END method updateBarcodes

    /** Get item information by barcode
     *
     * @param       enum        eLookupType     itemid or barcode(default)
     * @param       array       sLookupValues   array of lookup values
     * @return      mixed                       false on error, XML object of item information on success
     */
    private function getItem($eLookupType, $aLookupValues) {
        //API threshold met?
        if ($this->bAPIThresholdMet == true) {
            return false;
        }
        //continue
        $rCh = curl_init();
        $sURL = $this->aLoadedConfig['ex-libris-api-host'];
        //mode switch
        switch ($eLookupType) {
            case "itemid":
                if (!isset($aLookupValues['mms_id']) || !isset($aLookupValues['holding_id']) || !isset($aLookupValues['item_id'])) {
                    $this->sErrorReason = "Lookup value(s) not provided";
                    return false;
                }
                $aParamNames = array('{mms_id}', '{holding_id}', '{item_pid}');
                $aParamValues = array(urlencode($aLookupValues['mms_id']), urlencode($aLookupValues['holding_id']), urlencode($aLookupValues['item_id']));
                $sURL .= str_replace($aParamNames, $aParamValues, $this->aLoadedConfig['ex-libris-api-get-by-item-id']) . "?";
                break;
            default://report
                if (!isset($aLookupValues['barcode'])) {
                    $this->sErrorReason = "Lookup value(s) not provided";
                    return false;
                }
                $aParamNames = array('{item_barcode}');
                $aParamValues = array(urlencode($aLookupValues['barcode']));
                $sURL .= str_replace($aParamNames, $aParamValues, $this->aLoadedConfig['ex-libris-api-get-by-barcode']) . "&";
        }//END switch
        //create quest
        $sQuery = urlencode($this->aLoadedConfig['ex-libris-api-key-var-name']) . '=' . urlencode($this->aLoadedConfig['ex-libris-api-key']);
        curl_setopt($rCh, CURLOPT_URL, $sURL . $sQuery);
        curl_setopt($rCh, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($rCh, CURLOPT_HEADER, true);
        curl_setopt($rCh, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($rCh, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($rCh, CURLOPT_TIMEOUT, 400);
        curl_setopt($rCh, CURLOPT_FOLLOWLOCATION, true);
        $rResponse = curl_exec($rCh);
        if ($rResponse === false) {
            $this->sErrorReason = "cURL error: " . curl_error($rCh) . "cURL error no: " . curl_errno($rCh);
            return false;
        }
        $iResponse = curl_getinfo($rCh, CURLINFO_HTTP_CODE);
        $sBody = substr($rResponse, curl_getinfo($rCh, CURLINFO_HEADER_SIZE));
        $sHeader = substr($rResponse, 0, curl_getinfo($rCh, CURLINFO_HEADER_SIZE));
        //get X-Exl-Api-Remaining value and decide if we need to terminate this task
        $aHeaders = explode(PHP_EOL, $sHeader);
        foreach ($aHeaders as $sHeader) {
            if (substr($sHeader, 0, strlen($this->aLoadedConfig['ex-libris-api-threshold-header-variable'])) == $this->aLoadedConfig['ex-libris-api-threshold-header-variable']) {
                $iNumRequestRemaining = str_replace($this->aLoadedConfig['ex-libris-api-threshold-header-variable'], "", $sHeader);
                $iNumRequestRemaining = trim(ltrim($iNumRequestRemaining, ":"));
                if ($iNumRequestRemaining < $this->aLoadedConfig['ex-libris-api-request-threshold']) {
                    $this->sErrorReason = "API usage threshold meat at " . $iNumRequestRemaining;
                    $this->bAPIThresholdMet = true;
                    return false;
                }
            }//END if header line of interest
        }//END foreach header returned
        curl_close($rCh);
        //return XML
        if ($iResponse == 200) {
            $oXML = @simplexml_load_string($sBody);
            return $oXML;
        }
        else {
            $this->sErrorReason = http_build_query($aLookupValues, '', ',')  . "|Error HTTP response code: " . $iResponse;
            return false;
        }
    }//END method getItem


    /** Update item information
     *
     * @param       object      oXML    XML object of item information
     * @return      boolean             false on error, true on success
     */
    private function updateItem($oXML) {
        //API threshold met?
        if ($this->bAPIThresholdMet == true) {
            return false;
        }
        //pull our MMSID, Holdings ID, and Item ID
        $sMMSId = isset($oXML->bib_data, $oXML->bib_data->mms_id) ? (int)$oXML->bib_data->mms_id : false;
        $sHoldingId = isset($oXML->holding_data, $oXML->holding_data->holding_id) ? (int)$oXML->holding_data->holding_id : false;
        $sItemPID = isset($oXML->item_data, $oXML->item_data->pid) ? (int)$oXML->item_data->pid : false;
        //updatable?
        if ($sMMSId === false || $sHoldingId === false || $sItemPID === false) {
            $this->sErrorReason = "MMS Id, Holding Id and/or Item Id weren't found in Item Object";
            return false;
        }
        else {
            //save XML as a string
            $oDom = dom_import_simplexml($oXML)->ownerDocument;
            $oDom->formatOutput = true;
            $sXML = $oDom->saveXML();
            //perform update
            $rCh = curl_init();
            $sURL = $this->aLoadedConfig['ex-libris-api-host'] . $this->aLoadedConfig['ex-libris-api-update-by-item-id'];
            $aParamNames = array('{mms_id}','{holding_id}','{item_pid}');
            $aParamValues = array(urlencode($sMMSId),urlencode($sHoldingId),urlencode($sItemPID));
            $sURL = str_replace($aParamNames, $aParamValues, $sURL);
            $sQuery = '?' . urlencode($this->aLoadedConfig['ex-libris-api-key-var-name']) . '=' . urlencode($this->aLoadedConfig['ex-libris-api-key']);
            curl_setopt($rCh, CURLOPT_URL, $sURL . $sQuery);
            curl_setopt($rCh, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($rCh, CURLOPT_HEADER, false);
            curl_setopt($rCh, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($rCh, CURLOPT_POSTFIELDS, $sXML);
            curl_setopt($rCh, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($rCh, CURLOPT_TIMEOUT, 400);
            curl_setopt($rCh, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
            $rResponse = curl_exec($rCh);
            $iResponse = curl_getinfo($rCh, CURLINFO_HTTP_CODE);
            curl_close($rCh);
            //okay?
            if ($iResponse == 200) {
                return true;
            }//END success
            else {
                //get error code and emssage
                $oResponse = @simplexml_load_string($rResponse);
                $this->sErrorReason = isset($oResponse->errorList, $oResponse->errorList->error, $oResponse->errorList->error->errorCode, $oResponse->errorList->error->errorMessage) ? trim((string)$oResponse->errorList->error->errorCode) . ": " . trim((string)$oResponse->errorList->error->errorMessage) : "unknown";
                return false;
            }//END else error
        }//END else, Id's were found
    }//END method updateItem

    /** Validates header inforamtion against configuration
     *
     * @param       string      sHeader     Header to analyze
     * @param       array      sHeader      Array of header information from configuration
     * @return      boolean                 false on error, array of data mapping to header on success
     */
    private function headerValidationAndMapping($sHeader, $aConfigHeader){
        //check headers exist in first line of the file
        $mInputHeader = explode("|", trim($sHeader));
        //same amount of data elements?
        if (count($mInputHeader) < count($aConfigHeader)) {
            $this->sErrorReason = "Not enough data elements were provided";
            return false;
        }
        //lowercase input headers
        $mInputHeader = array_map('strtolower', $mInputHeader);
        //continue to verify
        $aHeaderMapping = array();
        foreach ($aConfigHeader as $sRequiredHeaderField) {
            if (!in_array(strtolower($sRequiredHeaderField), $mInputHeader)) {
                $this->sErrorReason = "Not all required data elements were provided(" . implode(";", $aConfigHeader) . ")";
                return false;
            }
            else {//build index key mapping to input file
                //map required fields to mapping array as keys
                foreach ($aConfigHeader as $sValue) {
                    $aHeaderMapping[strtolower($sValue)] = array_search(strtolower($sValue), $mInputHeader);
                }
            }//END else mapping
        }//END foreach header field
        return $aHeaderMapping;
    }//END method headerValidationAndMapping
}//END class ItemDescription


