# Executable Scripts
Location of executable scripts

## Report.php
Provide this script a pipe(|) delimited input file of barcodes and item descriptions which will br parsed and processed

### Command

```php -f Report.php ./resources/input/report/{input file name}```

Input files first line must contain the following headings in no particular order:

* barcode
* description

#### Example
``Description|Barcode``

## UpdateBarcode.php
Provide this script a pipe(|) delimited input file of barcodes that need updating

### Command(report mode, will not update items)
```php -f UpdateBarcode.php ./resources/input/update-barcodes/{input file name}```

### Command(commit mode, will update items)
```php -f UpdateBarcode.php ./resources/input/update-barcodes/{input file name} --commit```

Input files first line must contain the following headings in no particular order:

* mmsid
* holdingid
* itemid
* new barcode

#### Example
``MMSId|HoldingId|ItemId|New Barcode``

## UpdateItem.php
Provide this script a pipe(|) delimited input file of item information for updating

### Command(report mode, will not update items)
```php -f UpdateItem.php ./resources/input/update-item/{input file name}```

### Command(commit mode, will update items)
```php -f UpdateItem.php ./resources/input/update-item/{input file name} --commit```

Input files first line must contain the following headings in no particular order:

* enum a
* enum b
* enum c
* enum d
* enum e
* enum f
* enum g
* enum h
* chron i
* chron j
* chron k
* chron l
* chron m
* description
* barcode

#### Example
``Enum A|Enum B|Enum C|Enum D|Enum E|Enum F|Enum G|Enum H|Chron I|Chron J|Chron K|Chron L|Chron M|Description|Barcode``