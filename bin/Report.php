#!/usr/local/bin/php
<?php
//file provided?
if (!isset($argv, $argv[1])) {
    exit ("File not provided.\nExiting....\n");
}
//run
require __DIR__ . "/../src/ItemDescription.php";
$oProcessor = new ItemDescription();
$mResult = $oProcessor->generateReport($argv[1]);
if ($mResult === false) {
    exit($oProcessor->sErrorReason . "\n");
}
//save
$aInputFile = explode("/", $argv[1]);
$aInputFile = explode(".", $aInputFile[count($aInputFile)-1]);
//make report directory, if needed
$sInputFile = $aInputFile[0];
$sDirectory =  __DIR__ . "/../resources/output/report/" . $sInputFile . "/";
if (!file_exists($sDirectory)) {
    echo "Creating directory: " .  $sDirectory . "\n";
    mkdir($sDirectory, 0755);
}
//establish output file name
$sOutputFile = $sInputFile . "--" . date("Y-m-d_H-i-s") . ".txt";
//write file
file_put_contents($sDirectory . $sOutputFile, $mResult);
//echo
echo "Report written to : " . $sDirectory . $sOutputFile . "\n";
echo "Done!\n";