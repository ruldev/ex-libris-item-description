#!/usr/local/bin/php
<?php
//file provided?
if (!isset($argv, $argv[1])) {
    exit ("File not provided.\nExiting....\n");
}
$eMode = isset($argv, $argv[2]) && $argv[2] == "--commit" ? "commit" : "report";
//output start...
echo "Processing " . $argv[1] . " in " . $eMode . " mode.\n";

//run
require __DIR__ . "/../src/ItemDescription.php";
require_once __DIR__ . "/../config/config.php";
$oProcessor = new ItemDescription();
$oProcessor->loadConfig($aConfig);
$mResult = $oProcessor->updateItems($argv[1], $eMode);
if ($mResult === false) {
    exit($oProcessor->sErrorReason . "\n");
}
else {
    //save
    $aInputFile = explode("/", $argv[1]);
    $aInputFile = explode(".", $aInputFile[count($aInputFile)-1]);
    //make report directory, if needed
    $sInputFile = $aInputFile[0];
    $sDirectory =  __DIR__ . "/../resources/output/update-item/" . $sInputFile . "/";
    if (!file_exists($sDirectory)) {
        echo "Creating directory: " .  $sDirectory . "\n";
        mkdir($sDirectory, 0755);
    }
    //establish output file name
    $sResultsFile = $sInputFile . "--results--" . $eMode . "_mode--" . date("Y-m-d_H-i-s") . ".txt";
    $sErrorsFile = $sInputFile . "--errors--" . $eMode . "_mode--" . date("Y-m-d_H-i-s") . ".txt";
    //write files
    file_put_contents($sDirectory . $sResultsFile, $mResult['results']);
    file_put_contents($sDirectory . $sErrorsFile, $mResult['errors']);
    //echo
    echo "Results written to : " . $sDirectory . $sResultsFile . "\n";
    echo "Errors written to : " . $sDirectory . $sErrorsFile . "\n";
}//END else
echo "Done!\n";